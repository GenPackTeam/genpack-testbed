require 'docker'
require 'redis-objects'
require 'logger'
require 'colorize'
require_relative 'container'
require_relative 'node'
require_relative 'redis_service'
require_relative 'resque/jobs/job'
require_relative 'settings'
require 'pp'

class Genpack
  SWARM_IMAGE = 'genpack/swarm:1.2.0'
  MIGRATE_INTERVAL = 60 # seconds
  CONTAINER_AGE = 120

  ROADMAP = {
    nursery: "young",
    young: "tenured"
  }

  extend Forwardable
  def_delegators :@containers, :[], :first

  # store containers hash using gem redis-objects (https://github.com/nateware/redis-objects)
  include Redis::Objects
  list :containers_set
  list :counters, marshal: true

  def initialize(url)
    Docker.url = url
    @containers = {}

    @logger = Logger.new("#{File.dirname(__FILE__)}/../log/genpack.log")
  end

  # generate ID for redis-objects
  def id
    'genpack'
  end

  def infos(refresh=false)
    Redis.current.del 'genpack:infos' if refresh
    get_infos
  end

  def create(name=nil, params={}, docker_container=nil, run_listener=false)
    @logger.info "Genpack#create #{name} #{params.inspect} #{docker_container.inspect}"
    container = Container.new(name, params, docker_container)
    Resque.enqueue(ContainerListener, container.id, params[:type], container.info["Node"]["Name"], Time.now.to_i) if run_listener
    containers_set << container.id

    container
  end

  def start(container,opts={})
    @logger.info "Start container #{container.inspect} with opts #{opts.inspect}".colorize(:blue)
    id = get_id(container)
    container = Container.get(id)

    container.start

    # container.fetch
  end

  def clean
    Docker::Container.all(all: true, filters: { status: %w( created exited dead ) }.to_json)
      .each { |c| c.delete }
  end

  def containers(status: nil, generation: nil, upto: nil)
    filters = {}
    filters[:status] = [status] if status
    # filter 'label' not implemented in Swarm
    # filters[:label] = ["com.docker.swarm.constraints=[\"generation==#{generation}\"]"] if generation

    docker_containers = Docker::Container.all(all: true, filters: filters.to_json)

    docker_containers.map do |dc|
      next nil if dc.info["Image"] == SWARM_IMAGE # do not consider Swarm containers

      begin
        Container.get(dc.id)
      rescue Container::RedisNotFoundError, Docker::Error::NotFoundError => e
        @logger.error "Error Genpack#containers(status: #{status.inspect}, generation: #{generation.inspect}, upto: #{upto.inspect})\n\t-> with arg dc: #{dc.inspect}\n\t-> when mapping #{docker_containers.inspect}\n\nDocker::Container.get(dc.id): #{(Docker::Container.get(dc.id) rescue nil).inspect}".colorize(:magenta)
        nil
      end
    end
    .compact
    .select do |container|
      (
        status.nil? || [status].flatten.include?(container.info["State"]["Status"]) rescue false
      ) && (
        generation.nil? || container.info["Node"]["Name"].index(generation) == 0 rescue false
      ) && (
        upto.nil? || Time.parse(container.info["Created"]).to_i < upto rescue false
      )
    end
  end

  def containers_inspect
    load_from_redis
    @containers
  end

  def load_from_redis
    containers_set.each do |id|
      container = Container.get id

      unless container
        containers_set.delete id
        next
      end

      @containers[id] = container
    end

    @containers.values
  end

  def containers_from_image(*images)
    Docker::Container.all(all: true, filters: { ancestor: images }.to_json)
  end

  def swarm_containers
    Docker::Container.all(all: true, filters: { status: %w( created restarting running paused exited dead ) }.to_json)
      .reject { |c| c.info["Image"] == SWARM_IMAGE }
  end

  def migrate(from_generation)
    @logger.info "Genpack#migrate for generation #{from_generation}"

    to_generation = ROADMAP[from_generation.to_sym]

    generation_nodes = nodes_for_generation(generation: to_generation)
    @logger.debug "Genpack#migrate: available nodes for generation #{to_generation.inspect}: #{generation_nodes.inspect.colorize(:yellow)}"

    # select containers running for more than CONTAINER_AGE in selected generation
    containers = genpack.containers(status:"running", generation: from_generation, upto: (Time.now - CONTAINER_AGE).to_i)
    @logger.debug "Containers set to be migrated:\n#{containers.inspect.colorize(:yellow)}"

    containers_resources_sum = containers.inject({ cpu_sum: 0, mem_sum: 0 }) do |sum,container|
      { cpu_sum: sum[:cpu_sum] + container.cpu, mem_sum: sum[:mem_sum] + container.mem }
    end
    @logger.debug "Containers resources sum:\n#{containers_resources_sum.inspect.colorize(:magenta)}"

    containers.sort_by!{ |container| container.score(containers_resources_sum) }.reverse!
    @logger.debug "Containers set sorted by score:\n#{containers.inspect.colorize(:yellow)}"

    # do the trick about dispatching here
    containers.each do |container|
      if ['removing', 'removed'].include? container.status
        @logger.warn "Genpack#migrate: container #{container.inspect} already removed - escaping...".colorize(:red)
        next
      end

      # sort nodes by availability
      generation_nodes.sort_by!{ |node| node.availability }
      @logger.debug "Sorted nodes: #{generation_nodes.inspect}"

      selected_node = generation_nodes.detect{ |node| node.matches(cpu: container.cpu, memory: container.mem) }
      @logger.debug "Container migration: select node #{selected_node.inspect.colorize(:yellow)} for cpu: #{container.cpu}, memory: #{container.mem} - #{container.inspect}".colorize(:blue)

      unless selected_node
        @logger.warn "Genpack#migrate: no node available for container #{container.inspect} - escaping...".colorize(:red)
        next
      end

      selected_node.reserve(name: container.name, cpu: container.cpu, memory: container.mem)

      @logger.debug "Enqueue migration job for container named #{container.name}, current ID: #{container.id}, from generation #{from_generation.inspect} to #{to_generation}".colorize(:blue)
      Resque.enqueue(Migrate, container.name, container.id, to_generation, selected_node.node)
    end
  end

  def ps(generation: nil, upto: nil) # upto given in s
    containers = {}
    containers_set.values.each do |id|
      begin
        container = Container.get(id)
        (containers[container.info["Node"]["Name"]] ||= []) << container.inspect
      rescue => e
        containers_set.delete id
        @logger.warn "Genpack#ps -> Bug for container ID #{id} | (#{e.inspect})"
      end
    end
    containers.sort
    .select{ |k,v| k.index(generation.to_s) == 0 }
    .map{ |k,v| [k, v.select{ |c| upto.nil? || Time.parse(c.info["Created"]).to_i < upto }] }
    .to_h
  end

  def find_by_name(name)
    begin
      # id = Docker::Container.get(name).id
      # self[id]
      Container.get_by_name(name)

    rescue Docker::Error::NotFoundError => e
      @logger.warn "Container '#{name}' not found"
      raise e
    end
  end

  def nodes_for_generation(generation: nil)
    get_infos
      .select{ |k,v| k.index(generation.to_s) == 0 && v.is_a?(Hash) && v.include?("ip") }
      .map do |k,v|
        cpus = v["Reserved CPUs"].match /(\d*)\s\/\s(\d*)/
        cpus_reserved = cpus[1].to_i
        cpus_total = cpus[2].to_i

        memory = v["Reserved Memory"].match /(((\d|\.)*)\s(Gi|Mi|Ki)?B)\s\/\s(((\d|\.)*)\s(Gi|Mi|Ki)?B)/
        memory_reserved = memory_to_bytes(memory[1])
        memory_total = memory_to_bytes(memory[5])

         Node.new(
          node: k,
          ip: v["ip"],
          cpus_total: cpus_total,
          memory_total: memory_total
        )
      end
      .sort{ |x,y| x.availability <=> y.availability }
  end

  def nodes_by_memory(generation: nil)
    nodes_for_generation(generation: generation).values.sort { |x,y| x[:memory_available] <=> y[:memory_available] }
  end

  def nodes_by_cpu(generation: nil)
    nodes_for_generation(generation: generation).values.sort { |x,y| x[:cpus_available] <=> y[:cpus_available] }
  end

  def self.activated?
    Redis.current.get("genpack:experiment:migrate_activated") == "1"
  end

  private
    # proxyfy
    def method_missing(method, *args, &block)
      @logger.warn 'needs this instance method'
      @logger.warn method
      @logger.warn caller.join("\n").colorize(:yellow)
      super(method, *args, &block)
    end

    def self.method_missing(method, *args, &block)
      if Docker.respond_to?(method.to_sym)
        Docker.send(method, *args, &block)
      else
        super(method, *args, &block)
      end
    end

    def get_infos
      redis_infos = Redis::Value.new('genpack:infos')
      return JSON.parse(redis_infos.value) if redis_infos.value

      @infos = {}
      docker_infos = Docker.info["DriverStatus"]
      current_node = nil

      docker_infos.each do |k,v|
        case k
        when /\u0008(\w*)/
          @infos[$1] = v
        when /\s└\s((\w|\s)*)/
          @infos[current_node][$1] = v
        else
          current_node = k
          @infos[current_node] = {}
          @infos[current_node]['ip'] = v
        end
      end
      redis_infos.value = @infos.to_json
      @infos
    end

    def get_id(container)
      case container
      when Container
        container.id
      when String
        container
      else
        raise "Bad argument: #{container.inspect}"
      end
    end

    def memory_to_bytes(memory)
      match = memory.match /(((\d|\.)*)\s(Gi|Mi)?B)/

      if match
        (match[2].to_f * case match[4]
          when "Ki"
            1000
          when "Mi"
            1000000
          when "Gi"
            1000000000
          else
            1
          end).to_i
      else
        0
      end
    end
end
