# Resque tasks
require 'resque/tasks'
require 'resque/scheduler/tasks'
require 'yaml'

genpack_root = File.dirname(__FILE__) + '/../../'

# which workers ?
RESQUE_WORKERS = YAML.load_file(genpack_root + 'config/resque_launch.yml')

namespace :resque do
  task :setup do
    require 'resque'
    require 'resque/scheduler'
    require_relative '../resque/jobs/job'

    # you probably already have this somewhere
    genpack_root = File.dirname(__FILE__) + '/../../'

    # set logger
    Resque.logger = Logger.new(ENV["LOGFILE"] || "#{genpack_root}/log/resque.log")
    Resque.logger.level = Logger::DEBUG

    # resque_config = YAML.load_file(genpack_root + '/config/resque.yml')
    # Resque.redis = resque_config[rails_env]


    # The schedule doesn't need to be stored in a YAML, it just needs to
    # be a hash.  YAML is usually the easiest.
    # Resque.schedule = YAML.load_file(genpack_root + 'config/resque_schedule.yml') || {}
  end
end

# Start a worker with proper env vars and output redirection
def run_worker(queue, count=1, name=nil)
  genpack_root = File.dirname(__FILE__) + '/../../'

  log_dir = genpack_root + 'log/'
  FileUtils.mkdir_p log_dir
  pids_dir = genpack_root + 'tmp/pids/'
  FileUtils.mkdir_p pids_dir

  puts "Starting #{count} worker(s) #{name} with QUEUE: #{queue}"
  env_vars = {
    "QUEUE" => queue.to_s,
    "BACKGROUND" => "yes",
    "INTERVAL" => "1",
    "VERBOSE" => "1"
  }

  count.times do |pid_count|
    ops = {
      pgroup: true,
      err:    [(log_dir + "worker_#{name}_#{pid_count}_error.log").to_s, "a"],
      out:    [(log_dir + "worker_#{name}_#{pid_count}.log").to_s, "a"]
    }

    pid_path = pids_dir + "resque_worker_#{name}_#{pid_count}.pid"
    env_vars["PIDFILE"] = pid_path
    env_vars["LOGFILE"] = log_dir + "worker_#{name}_#{pid_count}.log"

    ## Using Kernel.spawn and Process.detach because regular system() call would
    ## cause the processes to quit when capistrano finishes

    pid = spawn(env_vars, "rake resque:work", ops)
    Process.detach(pid)
  end
end

# Start a scheduler, requires resque_scheduler >= 2.0.0.f
def run_scheduler
  genpack_root = File.dirname(__FILE__) + '/../../'

  puts "Starting resque scheduler"

  env_vars = {
    "BACKGROUND" => "yes",
    "PIDFILE" => (genpack_root + "tmp/pids/resque_scheduler.pid").to_s,
    "LOGFILE" => (genpack_root + "log/scheduler.log").to_s,
    "VERBOSE" => "1"
  }
  ops = {:pgroup => true, :err => [(genpack_root + "log/scheduler_error.log").to_s, "a"],
                          :out => [(genpack_root + "log/scheduler.log").to_s, "a"]}


  pid = spawn(env_vars, "rake resque:scheduler", ops)
  Process.detach(pid)
end

namespace :queues do
  # task :setup

  desc "Stop Everything in Resque (workers and scheduler)"
  task :stop_resque do
    Rake::Task['queues:stop_workers'].invoke
    Rake::Task['queues:stop_scheduler'].invoke
  end

  desc "Restart Everything in Resque (workers and scheduler)"
  task :restart_resque do
    Rake::Task['queues:restart_workers'].invoke
    Rake::Task['queues:restart_scheduler'].invoke
  end

  desc "Restart running workers"
  task :restart_workers do
    Rake::Task['queues:stop_workers'].invoke
    Rake::Task['queues:start_workers'].invoke
  end

  desc "Quit running workers"
  task :stop_workers do
    pids = Array.new
    Resque.workers.each do |worker|
      pids.concat(worker.worker_pids)
    end
    if pids.empty?
      puts "No workers to kill"
      true
    else
      syscmd = "kill -s KILL #{pids.join(' ')}"
      puts "Running syscmd: #{syscmd}"
      system(syscmd)
    end
  end

  desc "Start workers"
  task :start_workers do
    RESQUE_WORKERS.each do |worker|
      run_worker(worker['queues'], worker['workers_count'], worker['name'])
    end
  end

  desc "Restart scheduler"
  task :restart_scheduler do
    Rake::Task['queues:stop_scheduler'].invoke
    Rake::Task['queues:start_scheduler'].invoke
  end

  desc "Quit scheduler"
  task :stop_scheduler do
    genpack_root = File.dirname(__FILE__) + '/../../'
    pidfile = genpack_root + "tmp/pids/resque_scheduler.pid"
    if !File.exists?(pidfile)
      puts "Scheduler not running"
      true
    else
      pid = File.read(pidfile).to_i
      syscmd = "kill -s QUIT #{pid}"
      puts "Running syscmd: #{syscmd}"
      system(syscmd)
      FileUtils.rm_f(pidfile)
    end
  end

  desc "Start scheduler"
  task :start_scheduler do
    run_scheduler
  end

  desc "Reload schedule"
  task :reload_schedule do
    genpack_root = File.dirname(__FILE__) + '/../../'
    pidfile = genpack_root + "tmp/pids/resque_scheduler.pid"

    if !File.exists?(pidfile)
      puts "Scheduler not running"
      true
    else
      pid = File.read(pidfile).to_i
      syscmd = "kill -s USR2 #{pid}"
      puts "Running syscmd: #{syscmd}"
      system(syscmd)
    end
  end
end
