require 'optparse'
require 'yaml'
require_relative '../genpack'

CPU_FACTOR = 32 # number of CPU
MEM_FACTOR = 32 # GB of RAM

def genpack
  Genpack.new "tcp://localhost:2380"
end

def logger
  Logger.new("#{File.dirname(__FILE__)}/../../log/rake.log")
end

namespace :genpack do
  desc "Run container"
  task :run do
    tries = 3
    params = {}

    OptionParser.new do |opts|
      opts.banner = "Usage:"
      opts.on("-nNAME", "--name=NAME", "Container name") { |name| params[:name] = name }
      opts.on("-cCPU", "--cpu=CPU", "Number of cpus") { |cpu| params[:cpu] = cpu.to_f }
      opts.on("-mMEM", "--mem=MEM", "Number of memory") { |mem| params[:mem] = mem.to_f }
      opts.on("-tTYPE", "--type=TYPE", "Container type") { |type| params[:type] = type }
      opts.on("--migrate", "Will migrate the container after #{Genpack::MIGRATE_INTERVAL} seconds") { |migrate| params[:migrate] = migrate }
      opts.order!(ARGV) {}
    end.parse!

    cpu_qty = params[:cpu].to_i + 1

    params[:Cmd] = "-r fixed -n #{cpu_qty} -m #{(params[:mem] * 1000).to_i}MB -c #{(params[:cpu] * 100 / cpu_qty).to_i}".split

    logger.info params.inspect
    # exit

    begin
      c = genpack.create(nil, params)
    rescue Docker::Error::TimeoutError, Docker::Error::ConflictError => e
      logger.error "Docker error #{e.inspect}"
      dc = Docker::Container.get params[:name]
      c = genpack.create(nil, params, dc)

    # rescue Docker::Error::NotFoundError
    #   retry unless (tries -= 1).zero?
    end

    logger.info c.inspect

    # lock waiting ContainerListener to be run
    until Redis.current.get(c.id) == "1" do
      sleep 1
    end

    genpack.start(c, params)
    Redis.current.del(c.id)

    puts c.id
    # puts "Swarm state:\n#{JSON.pretty_generate genpack.ps}"

    exit
  end


  desc "Stop container by name"
  task :stop do
    params = {}

    OptionParser.new do |opts|
      opts.banner = "Usage:"
      opts.on("-nNAME", "--name=NAME", "Container name") { |name| params[:name] = name }
      opts.order!(ARGV) {}
    end.parse!

    c = genpack.find_by_name(params[:name])
    c.stop
    puts c.id

    exit
  end


  desc "Remove container by name"
  task :remove do
    params = {}

    OptionParser.new do |opts|
      opts.banner = "Usage:"
      opts.on("-nNAME", "--name=NAME", "Container name") { |name| params[:name] = name }
      opts.order!(ARGV) {}
    end.parse!

    c = genpack.find_by_name(params[:name])
    c.delete
    puts c.id

    exit
  end


  desc "Init genpacks data in Redis"
  task :init do
    Rake::Task["queues:stop_resque"].invoke

    # # delete all atomic counters
    # genpack.redis.keys('genpack:genpack:counter_*').each {|k| genpack.redis.del k}

    # # remove counters datas
    # genpack.counters.clear

    # flush all datas in redis
    genpack.redis.flushall

    # erase stopping datas log
    `rm -f #{StopContainer::STOPPING_DATAS_LOG}`
    `rm -f #{Migrate::SUCCEEDING_DATAS_LOG}`
    `rm -f #{Migrate::FAILING_DATAS_LOG}`

    Rake::Task["queues:start_workers"].invoke
    Rake::Task["queues:start_scheduler"].invoke

    puts "Genpack init: OK"
  end


  desc "Send counters in JSON"
  task :counters do
    puts genpack.counters.values.to_json
  end


  desc "Run jobs from jobs_sample.yml"
  task :jobs do
    # run listeners
    Redis.current.set "genpack:experiment:running", 1
    3.times do
      Resque.enqueue(ContainersGlobalListener)
      sleep 10
    end


    time = Time.now
    jobs = Hash[YAML::load_file(File.join(__dir__, '../../jobs_sample.yml'))]
    puts "Run #{jobs.size} jobs"

    jobs.each do |k,v|
      Resque.enqueue_at(time + (v[:start] / 1000000).to_i, RunContainer, "borg-#{k}", v[:cpu].to_f * CPU_FACTOR, v[:mem].to_f * MEM_FACTOR, v[:duration])
    end

    # Resque.enqueue_in(150, GenerationCycle, 'nursery')
    # Resque.enqueue_in(300, GenerationCycle, 'young')
    Resque.enqueue_at(time + (jobs.max_by{ |k,d| d[:end] }[1][:end] / 1000000).to_i, StopListeners)
  end
end
