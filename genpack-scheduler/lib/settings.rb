require 'yaml'

class Settings
  def self.create_section(name, settings)
    self.class.instance_eval do
      define_method(name) do
        settings
      end

      settings.each do |key, value|
        self.send(name).define_singleton_method(key) do
          value.freeze
        end
      end
    end
  end

  begin
    @@settings = YAML::load_file(File.dirname(__FILE__) + '/../config.yml')
  rescue Exception => e
    raise "Something is wrong with your config.yml file: #{e.message}"
  end

  @@settings.each do |key, value|
    create_section(key, value)
  end


  # add methods for nodes
  self.cluster.nodes.each do |node|
    [:ip, :cpu, :name, :group].each do |attribute|
      node.define_singleton_method(attribute) { node[attribute.to_s] }
    end
  end

  self.define_singleton_method(:manager) { self.cluster.manager }
  self.define_singleton_method(:manager_docker_port) { self.cluster.manager_docker_port }
  self.define_singleton_method(:node_docker_port) { self.cluster.node_docker_port }
  self.define_singleton_method(:manager_localhost) { "localhost:#{manager_docker_port}" }
  self.define_singleton_method(:node_localhost) { "localhost:#{node_docker_port}" }
  self.define_singleton_method(:nodes) { self.cluster.nodes }
end
