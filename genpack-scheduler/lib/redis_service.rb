require 'docker'
require 'redis'
require 'connection_pool'
require_relative 'settings'

module RedisService
  DOCKER_IMAGE = 'redis:3.2.1-alpine'
  DOCKER_URL = 'tcp://localhost:2375'
  DOCKER_HOST = Docker::Connection.new(DOCKER_URL, {})
  REDIS_HOST = 'localhost'
  REDIS_PORT = 6379
  POOL_SIZE = 5
  POOL_TIMEOUT = 5

  begin
    Docker::Container.get('redis', {}, DOCKER_HOST)
  rescue Docker::Error::NotFoundError
    puts 'No redis container found, start one'

    tries = 3

    begin
      redis = Docker::Container.create({
        'name' => 'redis',
        'Image' => DOCKER_IMAGE,
        'HostConfig' => {
          'PortBindings' => {
            '6379/tcp': [
              {
                'HostIp': REDIS_HOST,
                'HostPort': REDIS_PORT.to_s
              }
            ]
          }
        }
      }, DOCKER_HOST)

      puts redis.start

      Redis.current = ConnectionPool::Wrapper.new(size: POOL_SIZE, timeout: POOL_TIMEOUT) do
        Redis.new(host: REDIS_HOST, port: REDIS_PORT)
      end
    rescue Docker::Error::NotFoundError
      Docker::Image.create({ 'fromImage': DOCKER_IMAGE }, nil, DOCKER_HOST)
      retry unless (tries -= 1).zero?
    rescue Docker::Error::ConflictError
      c = Docker::Container.get('redis')
      c.delete
    end
  end

  def redis
    Redis.current
  end
  module_function :redis
end
