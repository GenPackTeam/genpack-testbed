require_relative '../genpack'

class Test
  @queue = :test_queue

  def self.logger
    Resque.logger
  end

  def self.perform()
    s = Genpack.new "tcp://localhost:2380"
    logger.info s.containers_set.values
  end
end
