require 'colorize'

class GenerationCycle < Job
  @queue = :generation_cycle

  def self.perform(generation)
    return unless Genpack.activated?

    gen = generation.to_sym

    logger.info "Starting generation cycle for generation #{generation.inspect} to #{Genpack::ROADMAP[generation.to_sym].inspect}, waiting all pending migration to be done..."
    until Resque.workers.map{ |w| w.job }.select{ |j| j["payload"] && j["payload"]["class"] == "Migrate" && j["payload"]["args"].include?(Genpack::ROADMAP[gen]) }.empty? && Resque.size("migrate").zero?
      sleep 5
    end
    logger.info "No pending migration, go for generation cycle!"

    containers = genpack.migrate(generation)

    logger.debug "GenerationCycle for generation #{generation.inspect} done, with containers:\n#{containers.inspect.colorize(:blue)}"
    # Resque.enqueue(GenerationCycle, generation)
  end
end
