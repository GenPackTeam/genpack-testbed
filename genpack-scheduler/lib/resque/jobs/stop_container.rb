require 'redis-objects'
require 'colorize'

class StopContainer < Job
  STOPPING_DATAS_LOG = "#{File.dirname(__FILE__)}/../../../log/stopping-datas.log"
  @queue = :stop_container
  @stopping_datas_logger = Logger.new(STOPPING_DATAS_LOG)

  @stopping_datas_logger.formatter = proc do |severity, datetime, progname, msg|
    "stop:#{datetime.to_time.to_i} #{msg}\n"
  end

  def self.perform(name, stop_timestamp)
    logger.info "Stop container with name: #{name}"

    status = Redis::Value.new(Container.status_redis_key(name))

    while ['migrating'].include?(status.value)
      logger.debug "Wait for stopping container #{name} because its current status is #{status.value}"
      sleep 5
    end

    status.value = 'removing'

    tries = 20
    delayed = false

    begin
      c = genpack.find_by_name(name)
      raise Exception unless c.is_a? Container

      until c.started?
        sleep 10
        c = genpack.find_by_name(name)
        raise Exception unless c.is_a? Container
      end

      unless delayed
        job_delay = Time.now.to_i - stop_timestamp.to_i
        delay = c.starting_delay # - job_delay
        logger.debug "Stop for container #{c.inspect} delayed for #{delay}s (container delay #{c.starting_delay}s - job delay #{job_delay}s)"
        sleep [delay, 0].max
        delayed = true
      end

      c.stop
    rescue NoMethodError => e
      logger.warn "#{e.class.inspect} while stopping container #{name}: #{e.inspect}"
      sleep Random.rand(10)
      retry unless (tries -= 1).zero?
      logger.error "#{e.class.inspect} while stopping container #{name}: give up :'('"
    rescue Docker::Error::NotFoundError => e
      logger.warn "#{e.class.inspect} while stopping container #{name}: #{e.inspect} - container should be already removed"
    rescue => e
      logger.warn "#{e.class.inspect} while stopping container #{name}: #{e.inspect} - #{tries > 1 ? 'retrying' : 'no retry left :O'}"
      retry unless (tries -= 1).zero?
    end

    logger.info "Stopping #{c.inspect.colorize(:blue)}"
    @stopping_datas_logger.info c.inspect

    # release resources if not yet done
    nodes = Node.release_for_job(name)
    logger.debug "Resources for job #{name} released on nodes #{nodes.inspect}"

    begin
      c = Docker::Container.get(name) unless c
      c.delete(force: true)
    rescue Docker::Error::ServerError => e
      logger.warn "Docker::Error::ServerError while stopping container #{name}, retrying..."
      c.stop rescue nil
      retry
    rescue Docker::Error::NotFoundError, Docker::Error::TimeoutError => e
      logger.warn "Docker::Error::NotFoundError||Docker::Error::TimeoutError (#{e.inspect}) while stopping container #{name}, container removed already"
    end

    status.value = 'removed'
    # release resources if not yet done (once again, just to be sure :])
    nodes = Node.release_for_job(name)
    logger.debug "Resources for job #{name} released on nodes #{nodes.inspect}"

    logger.info "Container with name: #{name} - #{c.id} has been stopped"
  end
end
