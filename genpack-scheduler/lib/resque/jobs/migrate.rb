require 'redis-objects'
require 'colorize'

class Migrate < Job
  FAILING_DATAS_LOG = "#{File.dirname(__FILE__)}/../../../log/failing-datas.log"
  SUCCEEDING_DATAS_LOG = "#{File.dirname(__FILE__)}/../../../log/succeeding-datas.log"
  @queue = :migrate
  @succeeding_datas_logger = Logger.new(SUCCEEDING_DATAS_LOG)
  @failing_datas_logger = Logger.new(FAILING_DATAS_LOG)

  @succeeding_datas_logger.formatter = proc do |severity, datetime, progname, msg|
    "succeeded:#{datetime.to_time.to_i} #{msg}\n"
  end

  @failing_datas_logger.formatter = proc do |severity, datetime, progname, msg|
    "failed:#{datetime.to_time.to_i} #{msg}\n"
  end

  def self.perform(name, container_id, to_generation, to_node)
    logger.info "Beginning 'Migrate' job for container #{container_id}"

    status = Redis::Value.new(Container.status_redis_key(name))

    if ['removing', 'removed'].include? status.value
      logger.info "No migration for container #{container_id} named #{name} because it is being removed - status: #{status.value}"
      return false
    end

    status.value = 'migrating'

    tries = 1

    begin
      logger.info "Starts migration to tenured generation for container name: #{name} ID: #{container_id}"

      container = Container.get(container_id)
      container = container.migrate(node: to_node, generation: to_generation)

      if container
        logger.info "Migration succeeded for container ID: #{container_id} -> #{container.inspect.colorize(:blue)}"
        @succeeding_datas_logger.info "name: #{name} ; container_id: #{container_id} ; to_generation: #{to_generation} ; to_node: #{to_node} ; container: #{container.inspect}"
      else
        logger.warn "Migration failed for container ID: #{container_id}".colorize(:orange)
        raise Exception, "Migration failed for container ID: #{container_id}"
      end

    rescue => e
      logger.error "Migration failed: #{e.inspect} -> retry once again"
      retry unless (tries -= 1).zero?

      logger.fatal "Migration really failed: #{e.inspect}"
      status.value = 'migration_failed'
      @failing_datas_logger.info "name: #{name} ; container_id: #{container_id} ; to_generation: #{to_generation} ; to_node: #{to_node} ; container: #{container.inspect}"

      # release resources
      node = Node.new(node: to_node)
      node.release(name: name)

      raise e
    end
  end
end
