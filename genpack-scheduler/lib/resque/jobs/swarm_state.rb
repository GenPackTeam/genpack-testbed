class SwarmState < Job
  @queue = :swarm_state

  def self.perform()
    logger.info "Swarm state:\n#{JSON.pretty_generate genpack.ps}"
  end
end
