require 'redis-semaphore'
require 'colorize'

class ContainersGlobalListener < Job
  @queue = :listeners

  def self.perform()
    genpack
    logger.info "Beginning 'ContainersGlobalListener' job on docker #{Docker.url}"

    listening = true

    while listening do
      begin
        Docker::Event.stream do |event|
          # sleep(Random.rand(50) / 100.to_f) # randomized delay to prevent concurrency on semaphore

          s = Redis::Semaphore.new("lock:#{event.id}:#{event.status}".to_sym, :host => "localhost")
          s.lock do
            # logger.info "Event received: #{event.inspect.colorize(:magenta)}"
            next if Redis.current.get "event:#{event.id}:#{event.status}"

            Redis.current.setex "event:#{event.id}:#{event.status}", 1, 30

            case event.status
            when 'start', 'die'
              logger.debug "Event #{event.status} caught: #{event.to_s.colorize(:magenta)}"
              handle(event)
            end
          end
        end
      rescue Docker::Error::TimeoutError, Excon::Errors::SocketError
        listening = (Redis.current.get("genpack:experiment:running") == "1")
        logger.warn "Stream TimeoutError, restart stream".colorize(:red) if listening
      end
    end
  end

  private
    def self.counter_sym(type, node)
      "counter_#{node}_#{type}".gsub('-', '_').to_sym
    end

    def self.handle(event)
      container_name = event.body["Actor"]["Attributes"]["name"]
      type = container_name.match(/(\w+)\-\d+/)[1]
      node_name = event.body["node"]["Name"]
      counter = counter_sym(type, node_name)

      # create counter dynamically if it does not exist
      unless genpack.respond_to?(counter)
        genpack.class.send(:counter, counter)
        logger.info "Create counter #{counter.inspect}: #{genpack.send(counter).value}"
      end

      case event.status
      when 'start'
        genpack.send(counter).incr
      when 'die'
        logger.debug "Event's body: #{event.body.inspect.colorize(:magenta)}"
        genpack.send(counter).decr

        unless node_name.include? 'nursery'
          # release resources
          node = Node.new(node: node_name)
          node.release(name: container_name)
        end
      end

      genpack.counters << { time: event.time, node: node_name, type: type, value: genpack.send(counter).value }
      # logger.info "Genpack counters state: #{genpack.counters.values.inspect.colorize(:green)}"
    end
end
