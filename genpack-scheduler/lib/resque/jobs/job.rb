require 'rake'
require 'resque'
require 'resque/scheduler'
require 'resque/scheduler/tasks'
require_relative '../../genpack'

class Job
  def self.logger
    Resque.logger.level = Logger::DEBUG
    Resque.logger
  end

  def self.genpack
    @@genpack ||= Genpack.new "tcp://localhost:2380"
  end
end

require_relative 'container_listener'
require_relative 'containers_global_listener'
require_relative 'generation_cycle'
require_relative 'migrate'
require_relative 'run_container'
require_relative 'stop_container'
require_relative 'stop_listeners'
require_relative 'swarm_state'
