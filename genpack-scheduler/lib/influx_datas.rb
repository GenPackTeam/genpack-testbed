require 'influxdb'
require_relative 'settings'

module InfluxDatas
  EPOCH = 'ms'

  # return
  # {
  #   timestamp: {
  #     serie1: value,
  #     serie2: value,
  #     ...
  #   },
  #   ...
  # }
  def self.datas_for_container(container, series)
    datas = {}
    last_timestamp = container.datas.keys.max
    since = last_timestamp || Time.parse(container.info["Created"]).to_i

    influxdb
    .query("
      SELECT * FROM #{series.join(',')} WHERE time > #{since}s AND time < #{Time.now.to_i}s AND container_name =~ /.*#{container.id}.*/
    ")
    .each do |serie|
      serie["values"].each do |value|
        datas[value["time"]] ||= {}
        datas[value["time"]][serie["name"]] = value["value"]
      end
    end

    datas
  end

  def self.enveloppes_for_container(container)
    since = Time.parse(container.info["Created"]).to_i

    query = "
      SELECT
        max(value) / (1024 * 1024 * 1024) as max,
        median(value) / (1024 * 1024 * 1024) as median,
        mean(value) / (1024 * 1024 * 1024) as mean,
        (percentile(value, 75) + (1.5 * (percentile(value, 75) - percentile(value, 25)))) / (1024 * 1024 * 1024) as upper_whisker,
        (percentile(value, 75) + (3 * (percentile(value, 75) - percentile(value, 25)))) / (1024 * 1024 * 1024) as max_whisker,
        percentile(value, 90) / (1024 * 1024 * 1024) as p90
      FROM memory_usage where container_name =~ /.*#{container.id}.*/ and time > #{since}s and time < #{Time.now.to_i}s
    "

    # puts query

    influxdb
    .query(query)
  end

  private
    def self.influxdb
      @influxdb_client ||= client
    end

    def self.client
      InfluxDB::Client.new 'cadvisor', epoch: EPOCH, retry: 10
    end
end
