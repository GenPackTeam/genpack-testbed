require 'redis-objects'
require 'colorize'

class Node
  @@logger = Logger.new("#{File.dirname(__FILE__)}/../log/genpack.log")
  @@logger.level = Logger::DEBUG

  attr_accessor :node, :ip, :resources, :containers

  def initialize(node:,ip:nil,cpus_total:nil,memory_total:nil)
    @node = node

    @ip = Redis::Value.new(key_for_field 'ip')
    @ip.value = ip if ip

    @resources = Redis::HashKey.new(key_for_field 'resources')
    @resources['cpus_total'] = cpus_total if cpus_total
    @resources['memory_total'] = memory_total if memory_total
    @resources['cpus_reserved'] = 0 unless @resources['cpus_reserved']
    @resources['memory_reserved'] = 0 unless @resources['memory_reserved']

    # @cpus_total = Redis::Value.new('cpus_total', cpus_total)
    # @memory_total = Redis::Value.new('memory_total', memory_total)
    # @containers = Redis::Value.new('containers', containers)

    @counter = Redis::Counter.new(key_for_field 'counter')
    @containers = Redis::List.new(key_for_field 'containers')
  end

  def key_for_field field
    "node:#{node}:#{field}"
  end

  def key_for_container container
    "node:#{node}:container:#{container}"
  end

  def availability
    Math.sqrt(available_resource_ratio('cpus') ** 2 + available_resource_ratio('memory') ** 2)
  end

  def reserve(name:, cpu:, memory:)
    @@logger.debug "Node#reserve node: #{self.inspect}, name: #{name.inspect}, cpu: #{cpu.inspect}, memory: #{memory.inspect}".colorize(:green)

    # persist reserved resources for container
    container_resources = Redis::HashKey.new(key_for_container name)
    container_resources['cpu'] = cpu
    container_resources['mem'] = memory * 2 ** 30

    @resources['cpus_reserved'] = @resources['cpus_reserved'].to_f + cpu
    @resources['memory_reserved'] = @resources['memory_reserved'].to_f + memory * 2 ** 30

    @counter.incr
    @containers << name

    @@logger.debug "Node status: #{self.inspect}".colorize(:green)
  end

  def release(name:)
    @@logger.debug "Node#release node: #{self.inspect}, name: #{name.inspect}".colorize(:green)

    # get persisted reserved resources for container
    container_resources = Redis::HashKey.new(key_for_container name)
    cpu = container_resources['cpu'].to_f
    memory = container_resources['mem'].to_f

    if cpu.zero? && memory.zero?
      @@logger.debug "Resources for container named #{name.inspect} already released on node #{self.inspect}".colorize(:green)
      return nil
    end

    # reset persisted resources for container
    container_resources['cpu'] = 0
    container_resources['mem'] = 0

    @@logger.debug "Resources to release for container named #{name.inspect} -> cpu: #{cpu.inspect}, memory: #{memory.inspect}".colorize(:green)

    @resources['cpus_reserved'] = @resources['cpus_reserved'].to_f - cpu
    @resources['memory_reserved'] = @resources['memory_reserved'].to_f - memory

    @counter.decr
    @containers.delete name

    @@logger.debug "Node status: #{self.inspect}".colorize(:green)

    self
  end

  def matches(cpu:, memory:)
    @@logger.debug "Node#matches node: #{self.inspect} -> cpu: #{cpu.inspect}, available_resource('cpus'): #{available_resource('cpus')}, memory: #{memory.inspect}, available_resource('memory') #{available_resource('memory')}".colorize(:green)
    result = (available_resource('cpus') >= cpu) && (available_resource('memory') >= (memory * 2 ** 30))
    @@logger.debug "Match result: #{result.inspect}".colorize(:green)
    return result
  end

  def reset_resources
    @resources['cpus_reserved'] = 0
    @resources['memory_reserved'] = 0
    @counter.reset
    @containers.clear
  end

  def inspect
    "<Node: @node=\"#{@node}\", @ip=\"#{@ip.value}\", @resources=\"#{@resources.all.inspect}\", @counter=\"#{@counter.value}\", @containers=\"#{@containers.values.inspect}\">"
  end

  def self.release_for_job(job_name)
    Redis.current.keys("node*container:#{job_name}")
    .map{ |k| k.match(/node:(.*):container/)[1] }
    .map{ |node| self.new(node:node).release(name:job_name) && node }
    .compact
  end

  def self.reset_node_resources(node_name)
    self.new(node:node_name).reset_resources
  end

  def self.infos
    puts [
      'nursery-node-1',
      'nursery-node-2',
      'nursery-node-3',
      'nursery-node-4',
      'nursery-node-5',
      'young-node-1',
      'young-node-2',
      'young-node-3',
      'young-node-4',
      'tenured-node-1',
      'tenured-node-2',
      'tenured-node-3'
    ].map{ |node|
      self.new(node:node).inspect
    }.join("\n")
  end

  # private
    def available_resource(resource)
      @resources["#{resource}_total"].to_f - @resources["#{resource}_reserved"].to_f
    end

    def available_resource_ratio(resource)
      available_resource(resource) / @resources["#{resource}_total"].to_f
    end
end
