require 'docker'
require 'redis-objects'
require_relative 'influx_datas'
require 'colorize'

class Container
  DOCKER_HOST = Docker::Connection.new('tcp://localhost:2380', {})

  # existing cAdvisor series:
  # cpu_usage_per_cpu, cpu_usage_system, cpu_usage_total, cpu_usage_user,
  # memory_usage, memory_working_set,
  # rx_bytes, rx_errors, tx_bytes, tx_errors

  METRICS = {
    cpu: {
      serie: 'cpu_usage_total',
      compute: ->(d1, d2) {
        (d2[:value] - d1[:value]) / ((d2[:timestamp] - d1[:timestamp]) * 1000000.0) rescue 0
      }
    },
    memory: {
      serie: 'memory_usage'
    }
  }
  TTL = 60
  REDIS_OLD_KEY_TTL = 1500

  @@logger = Logger.new("#{File.dirname(__FILE__)}/../log/genpack.log")
  @@logger.level = Logger::DEBUG

  class MissingAttributeError < Exception; end
  class RedisNotFoundError < Exception; end
  class MigrationError < Exception; end

  attr_accessor :container, :timestamp, :metrics, :datas, :cpu, :mem, :name, :created

  # store container using gem redis-objects (https://github.com/nateware/redis-objects)
  include Redis::Objects
  value :store


  # INITIALIZER

  def initialize(name=nil, params={}, docker_container=nil)
    tries = 20

    @name = name || params[:name]
    @created = (params[:created] || Time.now).to_i
    @start = params[:start].to_i
    @type = params[:type]

    @generation_current = params[:generation_current] || 'nursery'
    @generation_previous = params[:generation_previous]
    @generation_timestamps = params[:generation_timestamps] && params[:generation_timestamps].deep_symbolize || {}

    @image = params[:Image] || 'lorel/docker-lookbusy-supervisor'
    @args = params[:Cmd] || %w(-r fixed -n 1 -m 1GB -c 25)
    @host_config = params[:HostConfig] || {
      'Memory' => (params[:mem].to_f * 1000000000).to_i,
      'CpuShares' => params[:cpu].to_f.ceil,
      'AutoRemove' => true
    }

    @cpu = params[:cpu]
    @mem = params[:mem]

    # @datas keys have to be integer
    @datas = params[:datas] && params[:datas].integerize || {}

    # @metrics keys (and nested) have to be symbol
    @metrics = (
      params[:metrics] && params[:metrics].deep_symbolize ||
      Hash[METRICS.keys.map{ |s| [s, { max: 0, mean: 0 }] }]
    )

    @labels = params[:Labels] || {}
    set_generation('nursery') if Genpack.activated? && params[:node].nil?
    # set_node(params[:node])

    @status = Redis::Value.new(self.class.status_redis_key(@name))

    if docker_container || params[:id] || @status.value
      begin
        @container = docker_container || Docker::Container.get(params[:id] || params[:name], {}, DOCKER_HOST)
        @timestamp = params[:timestamp] && Time.at(params[:timestamp]) || Time.now
        fetch

      rescue => e
        @@logger.error "Container#new Error: #{e.inspect}".colorize(:yellow)
        @@logger.error e.backtrace.join("\n").colorize(:yellow)

        sleep 2
        retry unless (tries -= 1).zero?
        raise e
      end
    else
      create_container!
      @timestamp = Time.now
    end

    store!
  end


  # INSTANCE METHODS

  def status
    @status.value
  end

  def status=(value)
    @status.value = value
  end

  def start
    @container.start
    @start = Time.now.to_i
    start_timestamp
    self.status = 'started'
    store!
  end

  def stop
    tries = 10
    begin
      @container.stop
      stop_timestamp

      self.status = 'stopped'
    rescue Docker::Error::TimeoutError
      retry unless (tries -= 1).zero?
    end
  end

  def fetch
    return unless self.started?

    @@logger.debug "Call Container#fetch for Container #{self.inspect}\n#{caller.join("\n")}".colorize(:yellow)

    begin
      if Time.now - @timestamp > TTL
        @container = Docker::Container.get(id, {}, DOCKER_HOST)
        @timestamp = Time.now
      end

      @datas.merge! InfluxDatas.datas_for_container(self, METRICS.map{ |k,v| v[:serie] })

      @metrics.each do |key, hash|
        next if key == :enveloppes

        # @datas structure:
        # {
        #   timestamp: {
        #     serie1: value,
        #     serie2: value,
        #     ...
        #   },
        #   ...
        # }

        datas = if METRICS[key][:compute]
          timestamps = @datas.keys.sort
          @datas.map do |timestamp, values|
            if timestamp == timestamps.first
              0
            else
              prev_timestamp = timestamps[timestamps.index(timestamp) - 1]
              if @datas[timestamp][:node] == @datas[prev_timestamp][:node]
                METRICS[key][:compute].call(
                  { timestamp: prev_timestamp, value: @datas[prev_timestamp][METRICS[key][:serie]]},
                  { timestamp: timestamp, value: values[METRICS[key][:serie]]}
                )
              else
                0
              end
            end
          end
        else
          @datas.map { |timestamp, values| values[METRICS[key][:serie]] }
        end

        hash[:max] = datas.max || 0
        hash[:mean] = datas.empty? && 0 || datas.inject{ |sum, el| sum + el } / datas.size
      end

      # do not compute_enveloppes if container is too young (no data yet in InfluxDB) or if container is not in a monitored generation
      if self.started? && (@start + TTL) < Time.now.to_i && ['nursery', 'young'].include?(@generation_current)
        @@logger.debug "Container#fetch call Container#compute_enveloppes - @start: #{@start.inspect} - @start + TTL: #{(@start + TTL).inspect} - (@start + TTL) < Time.now.to_i: #{((@start + TTL) < Time.now.to_i).inspect}".colorize(:yellow)

        compute_enveloppes

        # update needed resources
        @mem = @metrics[:enveloppes]["#{@generation_previous}-memory_usage".to_sym][:p90] if @metrics[:enveloppes] && @metrics[:enveloppes]["#{@generation_previous}-memory_usage".to_sym] && @metrics[:enveloppes]["#{@generation_previous}-memory_usage".to_sym][:p90]
        @cpu = @metrics[:cpu][:mean]
      end

      store!
      self

    rescue Docker::Error::NotFoundError => e
      @@logger.warn e.inspect
      @@logger.warn "Container #{id} not found by docker daemon"
      return false

    rescue InfluxDB::ConnectionError => e
      @@logger.fatal e.inspect
      raise "Unable to connect to InfluxDB"
    end
  end

  def compute_enveloppes
    enveloppes = InfluxDatas.enveloppes_for_container(self)

    unless enveloppes.size == 1
      @@logger.warn "enveloppes have not been retrieved correctly for container #{container.to_s}: #{enveloppes.inspect}"
      return self
    end

    unless enveloppes.first["values"].size == 1
      @@logger.warn "values have not been retrieved correctly for container #{container.to_s}: #{enveloppes.first["values"].inspect}"
      return self
    end

    @@logger.info "enveloppes values for container #{container.inspect}: #{enveloppes.inspect}"

    values = enveloppes.first["values"].first

    @metrics[:enveloppes] = {}
    @metrics[:enveloppes]["#{@generation_current}-#{enveloppes.first["name"]}".to_sym] = {
      time: values["time"],
      max: values["max"],
      median: values["median"],
      mean: values["mean"],
      upper_whisker: values["upper_whisker"],
      max_whisker: values["max_whisker"],
      p90: values["p90"]
    }

    self
  end

  def migrate(params)
    tries = 50
    # self.status = 'migrating'
    old_key = redis_field_key('store')

    begin
      # retrieve last informations
      fetch

      # stop and remove container
      stop

      begin
        @container.delete
      rescue Docker::Error::TimeoutError => e
        @@logger.warn "Docker::Error::TimeoutError while migrating container #{self.inspect} on delete - #{e.inspect}".colorize(:red)
        sleep 2
        retry unless (tries -= 1).zero?
      rescue Docker::Error::ServerError => e
        @@logger.warn "Docker::Error::ServerError while migrating container #{self.inspect} on delete - #{e.inspect} - try force delete".colorize(:red)
        @container.delete(force: true) rescue retry
      end

      # reset settings for resources reservation considered by Swarm
      @host_config['Memory'] = 0
      @host_config['CpuShares'] = 0

      # create new container with new node param if exists
      set_node(params[:node])
      set_generation(params[:generation])
      create_container!

      # start container
      begin
        @container.start
      rescue Docker::Error::TimeoutError, Docker::Error::NotFoundError => e
        @@logger.warn "Docker::Error::TimeoutError|NotFoundError while migrating container #{self.inspect} on start - #{e.inspect}".colorize(:red)
        sleep 2
        retry unless (tries -= 1).zero?
      end

      start_timestamp
      self.status = 'started'

      # replace old store value by new container ID
      redis.setex(old_key, REDIS_OLD_KEY_TTL, self.id)

      # store datas with new redis field key
      store!

      self

    rescue => e
      @@logger.error "MigrationError for #{self.inspect} with params #{params.inspect} -> #{e.inspect}".colorize(:red)
      raise MigrationError, "MigrationError for #{self.inspect} with params #{params.inspect} -> #{e.inspect}"
    end
  end

  def hash_params
    {
      'name'       => @name,
      'Cmd'        => @args,
      'Image'      => @image,
      'HostConfig' => @host_config,
      'Labels'     => @labels
    }.compact
  end

  def to_json(options = nil)
    hash_params.merge({
      type:                   @type,
      id:                     id,
      created:                @created,
      start:                  @start,
      cpu:                    @cpu,
      mem:                    @mem,
      datas:                  @datas,
      metrics:                @metrics,
      timestamp:              @timestamp.to_i,
      generation_timestamps:  @generation_timestamps,
      generation_current:     @generation_current,
      generation_previous:    @generation_previous
    }).to_json
  end

  def [](key)
    if self.respond_to?(key.to_sym)
      self.send(key)
    else
      raise NoMethodError
    end
  end

  def []=(key,value)
    if self.respond_to?("#{key}=".to_sym)
      self.send("#{key}=".to_sym, value)
    else
      raise NoMethodError
    end
  end

  def self.get(id, docker_container=nil)
    redis_key =  "#{self.to_s.downcase}:#{id}:store"
    @@logger.debug "Container#self.get id:#{id} redis_key:#{redis_key}".colorize(:yellow)
    redis_container = redis.get redis_key

    while redis_container =~ /^\h{64}$/ do # only Docker ID is stored, giving new container ID
      redis_key =  "#{self.to_s.downcase}:#{redis_container}:store"
      @@logger.debug "Container#self.get id:#{id} redis_key:#{redis_key}".colorize(:yellow)
      redis_container = redis.get redis_key
    end

    unless redis_container
      @@logger.error "Container #{id} not found in Redis with key #{redis_key}\n#{caller.join("\n")}".colorize(:red)

      raise RedisNotFoundError, "Container #{id} not found in Redis with key #{redis_key}"
    end

    hash = JSON.parse(redis_container)
    hash = hash.symbolize

    begin
      self.new(nil, hash, docker_container)
    rescue Docker::Error::NotFoundError
      @@logger.warn "Container #{id} not found by docker daemon"
      return nil
    end
  end

  def self.get_by_name(name)
    container = Docker::Container.get(name, {}, DOCKER_HOST)
    self.get(container.id, container)
  end

  def self.all
    self.redis.keys('container*store').map do |key|
      self.get key.match(/container:(.*):store/)[1]
    end
  end

  def self.status_redis_key(name)
    "container:status:#{name}"
  end

  def inspect
    "#<Container name:#{@name} ID:#{(@container && @container.id).inspect} start:#{@start} created:#{@created} generation_timestamps:#{@generation_timestamps.inspect} generation_current:#{@generation_current} cpu:#{@cpu} mem:#{@mem} metrics:#{@metrics.inspect}>"
  end

  def set_node(node_name)
    @labels["com.docker.swarm.constraints"] = ["node==#{node_name}"].to_json if node_name
  end

  def set_generation(generation)
    @generation_previous = @generation_current
    constraints = (@labels["com.docker.swarm.constraints"] ? JSON.parse(@labels["com.docker.swarm.constraints"]) : []).reject{ |constraint| constraint.include? "generation" }
    constraints << "generation==#{generation}"
    @labels["com.docker.swarm.constraints"] = constraints.to_json
    @generation_current = generation
  end

  def starting_delay
    @start - @created + migrating_time
  end

  def started?
    @start > 0
  end

  def migrating_time
    time = 0
    if @generation_timestamps[:young] && @generation_timestamps[:nursery][:stop]
      time += @generation_timestamps[:young][:start] - @generation_timestamps[:nursery][:stop]
    end
    if @generation_timestamps[:tenured] && @generation_timestamps[:young][:stop]
      time += @generation_timestamps[:tenured][:start] - @generation_timestamps[:young][:stop]
    end
    time
  end

  def score(cpu_sum:, mem_sum:)
    Math.sqrt((@cpu / cpu_sum) ** 2 + (@mem / mem_sum) ** 2)
  end

  private
    # proxyfy
    def method_missing(method, *args, &block)
      if @container.respond_to?(method.to_sym)
        begin
          @container.send(method, *args, &block)
        rescue => e
          @@logger.error "Method #{method} on Docker::Container raised an error"
          @@logger.error e.to_s
          raise e
        end
      else
        case method
        when :id
          nil
        else
          @@logger.warn 'needs this instance method'
          @@logger.warn method
          @@logger.warn args.inspect
          @@logger.warn caller
          super(method, *args, &block)
        end
      end
    end

    def create_container!
      tries = 10
      conflict = false

      begin
        Docker::Container.create(hash_params, DOCKER_HOST) unless conflict

        begin
          @container = Docker::Container.get(@name, {}, DOCKER_HOST)

        rescue Docker::Error::NotFoundError => e
          sleep Random.rand(5)
          @@logger.error "Cannot recover container #{self.inspect} (tries #{tries}) error: #{e.inspect} - try recreate"

          begin
            Docker::Container.create(hash_params, DOCKER_HOST)
          rescue Docker::Error::ConflictError => e
            @@logger.error "Cannot recreate container #{self.inspect} (tries #{tries}) error: #{e.inspect}"
          end

          retry unless (tries -= 1).zero?
          raise e
        end

      rescue Docker::Error::TimeoutError => e
        sleep Random.rand(10)
        retry unless (tries -= 1).zero?
        @@logger.error "Cannot recover container #{self.inspect} error: #{e.inspect}"
        raise e

      rescue Docker::Error::ConflictError => e
        conflict = true
        sleep Random.rand(10)
        retry unless (tries -= 1).zero?
        @@logger.error "Cannot recover container #{self.inspect} error: #{e.inspect}"
        raise e

      rescue Docker::Error::ServerError => e
        @@logger.warn "Docker::Error::ServerError while creating container #{self.inspect}: #{e.inspect}"
        retry unless e.inspect.include? "Container created but refresh didn't report it back"
      end

      @@logger.info "Create container: #{@container.inspect}"
      # node = Node.new(node: @container.info["Node"]["Name"])
      # node.reserve(name: @name, cpu: @cpu, memory: @mem)

      store.key = redis_field_key('store') # redefine Redis key with new container ID
      self.status = 'created'
    end

    def store!
      store.value = self.to_json
    end

    def start_timestamp
      (@generation_timestamps[@generation_current.to_sym] ||= {})[:start] = Time.now.to_i
    end

    def stop_timestamp
      (@generation_timestamps[@generation_current.to_sym] ||= {})[:stop] = Time.now.to_i
    end

    def use_genpack_resources
      unless @generation_current
        message = "@generation_current for Container #{self.inspect} has to be set"
        @@logger.error message.colorize(:red)
        raise MissingAttributeError, message
      end

      Genpack.activated? && @generation_current != 'nursery'
    end
end

Hash.class_eval do
  def compact
    delete_if { |k, v| v.nil? }
  end

  def symbolize
    inject({}) { |h,(k,v)| h[k.to_sym] = v ; h }
  end

  def deep_symbolize
    inject({}) { |h,(k,v)| h[k.to_sym] = v.is_a?(Hash) ? v.deep_symbolize : v ; h }
  end

  def integerize
    inject({}) { |h,(k,v)| h[k.to_i] = v ; h }
  end
end

Redis::BaseObject.class_eval do
  attr_accessor :key
end

# fix NameError: uninitialized constant Excon::Errors::InternalServerError
Excon::Errors::InternalServerError = Excon::Errors::InternalServerErrorError
