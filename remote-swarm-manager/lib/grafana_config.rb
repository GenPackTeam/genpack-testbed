def export_grafana_config
  FileUtils.mkdir_p('output')
  File.open('output/grafana.json','w') do |f|
    f.write(JSON.pretty_generate(config(machines)))
  end
end

def config nodes
  global(nodes.map{ |node| targets_by_machine(node).map{ |k,v|  row(node, k, v)} }.flatten)
end

def row machine, title, targets
  {
    "collapse": false,
    "editable": true,
    "height": "250px",
    "panels": [
      {
        "aliasColors": {},
        "bars": false,
        "datasource": nil,
        "editable": true,
        "error": false,
        "fill": 1,
        "grid": {
          "leftLogBase": 1,
          "leftMax": nil,
          "leftMin": nil,
          "rightLogBase": 1,
          "rightMax": nil,
          "rightMin": nil,
          "threshold1": nil,
          "threshold1Color": "rgba(216, 200, 27, 0.27)",
          "threshold2": nil,
          "threshold2Color": "rgba(234, 112, 112, 0.22)"
        },
        "id": 1,
        "legend": {
          "avg": false,
          "current": false,
          "max": false,
          "min": false,
          "show": true,
          "total": false,
          "values": false
        },
        "lines": true,
        "linewidth": 2,
        "links": [],
        "nilPointMode": "connected",
        "percentage": false,
        "pointradius": 5,
        "points": false,
        "renderer": "flot",
        "seriesOverrides": [],
        "span": 12,
        "stack": false,
        "steppedLine": false,
        "targets": targets,
        "timeFrom": nil,
        "timeShift": nil,
        "title": "#{title} - #{machine}",
        "tooltip": {
          "shared": true,
          "value_type": "cumulative"
        },
        "type": "graph",
        "x-axis": true,
        "y-axis": true,
        "y_formats": [
          "short",
          "short"
        ]
      }
    ],
    "title": "#{title} - #{machine}"
  }
end

def targets_by_machine machine
  {
    "CPU": [
      {
        "measurement": "cpu_usage_per_cpu",
        "query": "SELECT mean(value) FROM \"cpu_usage_per_cpu\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "cpu_usage_system",
        "query": "SELECT mean(value) FROM \"cpu_usage_system\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "cpu_usage_total",
        "query": "SELECT mean(value) FROM \"cpu_usage_total\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      }
    ],
    "FS": [
      {
        "measurement": "fs_usage",
        "query": "SELECT mean(value) FROM \"fs_usage\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "fs_limit",
        "query": "SELECT mean(value) FROM \"fs_limit\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      }
    ],
    "MEMORY": [
      {
        "measurement": "memory_usage",
        "query": "SELECT mean(value) FROM \"memory_usage\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "memory_working_set",
        "query": "SELECT mean(value) FROM \"memory_working_set\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      }
    ],
    "NETWORK": [
      {
        "measurement": "rx_bytes",
        "query": "SELECT mean(value) FROM \"rx_bytes\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "rx_errors",
        "query": "SELECT mean(value) FROM \"rx_errors\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "tx_bytes",
        "query": "SELECT mean(value) FROM \"tx_bytes\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      },
      {
        "measurement": "tx_errors",
        "query": "SELECT mean(value) FROM \"tx_errors\" WHERE $timeFilter AND machine='#{machine}' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      }
    ],
    "POWER": [
      {
        "measurement": "power",
        "query": "SELECT mean(value) FROM \"power\" WHERE $timeFilter AND machine='43e6672e0806' GROUP BY time($interval) ORDER BY asc",
        "tags": {
          "machine": machine
        }
      }
    ]
  }
end

def global rows
  {
    "id": 1,
    "title": "cAdvisor",
    "originalTitle": "cAdvisor",
    "tags": [],
    "style": "dark",
    "timezone": "browser",
    "editable": true,
    "hideControls": false,
    "sharedCrosshair": false,
    "rows": rows,
    "nav": [
      {
        "collapse": false,
        "enable": true,
        "notice": false,
        "now": true,
        "refresh_intervals": [
          "5s",
          "10s",
          "30s",
          "1m",
          "5m",
          "15m",
          "30m",
          "1h",
          "2h",
          "1d"
        ],
        "status": "Stable",
        "time_options": [
          "5m",
          "15m",
          "1h",
          "6h",
          "12h",
          "24h",
          "2d",
          "7d",
          "30d"
        ],
        "type": "timepicker"
      }
    ],
    "time": {
      "from": "now-15m",
      "to": "now"
    },
    "templating": {
      "list": []
    },
    "annotations": {
      "list": []
    },
    "schemaVersion": 6,
    "version": 1
  }
end
