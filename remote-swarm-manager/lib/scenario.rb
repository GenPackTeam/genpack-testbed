require 'colorize'
require 'redis'

class Scenario
  attr_reader :containers

  GENPACK_LOCAL_LOCATION = '../genpack-scheduler'
  GENPACK_REMOTE_LOCATION = '/home/ubuntu/genpack'

  SLEEP_BEFORE = 10
  SLEEP_AFTER = 10
  SLEEP_MARGIN = 30

  SLEEP_BEFORE_BENCH = 30
  SLEEP_AFTER_BENCH = 30

  def initialize(scenario)
    @scenario = scenario
    @containers = {
      global: {},
      cpu: {},
      mem: {},
      none: {}
    }
    @counter = {
      global: 0,
      cpu: 0,
      mem: 0,
      none: 0
    }
  end

  def self.generate(items_number)
    i = -1
    self.new begin
      Array.new(items_number).map do
        i += 1
        stressng = Stressng.new
        {
          args: stressng.args,
          opts: stressng.opts,
          timeout: stressng.timeout,
          sleep: 5 + (i % 5 == 0 ? 60 : 5) # rand(5000)/1000.0 + (i % 5 == 0 ? 60 : 5)
        }
      end
    end
  end

  def self.generate_determined()
    i = 0
    cpu_heavy = {
      cpu: 2,
      mem: 1,
      type: 'cpu'
    }

    mem_heavy = {
      cpu: 1,
      mem: 2,
      type: 'mem'
    }

    self.new begin
      [
        Array.new(5, cpu_heavy),
        Array.new(5, mem_heavy),
        Array.new(5, cpu_heavy),
        Array.new(5, mem_heavy)
      ].flatten.map do |params|
        i += 1
        params[:args] = "--cpu #{params[:cpu]} --cpu-load 100 --cpu-ops #{params[:cpu] * 20000} --cpu-method fibonacci --vm #{params[:mem]} --vm-bytes 900M --vm-ops #{params[:mem] * 20} --vm-method flip --metrics-brief"
        # params[:opts] = "--rm --memory #{params[:mem] * 900}M --cpu-shares #{params[:cpu]} -e constraint:type==#{params[:type]} --name #{params[:type]}-#{i}"
        params[:opts] = "-d --memory #{params[:mem] * 900}M --memory-swap 0 --cpu-shares #{params[:cpu]} --name #{params[:type]}-#{i}"
        stressng = Stressng.new(params)
        {
          args: stressng.args,
          opts: stressng.opts,
          type: params[:type],
          node: nil,
          timeout: 250,
          sleep: 5
        }
      end
    end
  end

  def run(strategy)
    puts "Waiting #{SLEEP_BEFORE} seconds before starting scenario".colorize(:yellow)
    sleep SLEEP_BEFORE

    start = Time.now
    threads = []

    @scenario.each_with_index do |params, index|
      params[:strategy] = strategy
      sleep params[:sleep]
      puts "\t--> Run container ##{index + 1}/#{@scenario.count} <--\n"
      threads << Thread.new do
        stressng = Stressng.new(params)
        incr_counter

        # begin
        #   Timeout::timeout(params[:timeout]) do
        #     puts "Run stressng with timeout #{params[:timeout]}s"
        #     puts stressng.run
        #     puts "Stressng done on time"
        #   end
        # rescue Timeout::Error
        #   puts "Container ##{index + 1} kill by timeout".colorize(:red)
        # end

        puts "Run stressng with timeout #{params[:timeout]}s"
        puts stressng.run
        puts "Stressng done"

        decr_counter
        puts "Container ##{index + 1} done at #{Time.now.to_s.colorize(:magenta)}"
      end
    end
    threads.each{ |thread| thread.join }

    puts "Waiting #{SLEEP_AFTER} seconds before stopping scenario".colorize(:yellow)
    sleep SLEEP_AFTER

    finish = Time.now

    puts "Scenario done, all containers have been run (duration: #{finish - start}s)".colorize(:red)
    # puts @containers.inspect

    sleep SLEEP_MARGIN

    {
      start: start,
      finish: finish
    }
  end


  def run_lookbusy(strategy)
    puts "Waiting #{SLEEP_BEFORE} seconds before starting scenario".colorize(:yellow)
    sleep SLEEP_BEFORE

    start = Time.now
    threads = []

    @scenario.each_with_index do |params, index|
      case params[:type]
      when "cpu"
        params[:cpu] = 2
        params[:mem] = 1
      when "mem"
        params[:cpu] = 1
        params[:mem] = 2
      else
        params[:cpu] = 1
        params[:mem] = 1
      end


      params[:strategy] = strategy
      sleep params[:sleep]
      puts "\t--> Run container ##{index + 1}/#{@scenario.count} <--\n"
      threads << Thread.new do
        lookbusy = Lookbusy.new(params)

        puts "Run lookbusy with timeout #{params[:timeout]}s"
        begin
          puts lookbusy.run
        end until lookbusy.container =~ /^\h{64}$/

        puts "Lookbusy run: #{lookbusy.container.colorize(:blue)}"
        incr_counter params[:type]

        sleep params[:timeout]

        puts lookbusy.stop
        decr_counter params[:type]
        puts "Container ##{index + 1} done at #{Time.now.to_s.colorize(:magenta)}"

        puts lookbusy.rm
      end
    end
    threads.each{ |thread| thread.join }

    puts "Waiting #{SLEEP_AFTER} seconds before stopping scenario".colorize(:yellow)
    sleep SLEEP_AFTER

    finish = Time.now

    puts "Scenario done, all containers have been run (duration: #{finish - start}s)".colorize(:red)

    sleep SLEEP_MARGIN

    {
      start: start,
      finish: finish
    }
  end

  def self.run_borg(genpack=false)
    puts "Send current Genpack source code to manager"
    puts ` scp -r #{GENPACK_LOCAL_LOCATION}/* #{Settings.manager}:/home/ubuntu/genpack/`

    puts "Init genpack"
    commands = [
      "bundle check || bundle install",
      "bundle exec rake queues:stop_resque",
      "bundle exec rake genpack:init"
    ]
    puts ssh_exec(Settings.manager, "cd #{GENPACK_REMOTE_LOCATION} && #{commands.join(' && ')}")

    open_gateway(Settings.manager, 6379)
    Redis.current.set("genpack:mode", 1)
    Redis.current.set("genpack:experiment:migrate_activated", 1) if genpack

    puts "Waiting #{SLEEP_BEFORE} seconds before starting scenario".colorize(:yellow)
    sleep SLEEP_BEFORE
    start = Time.now
    commands = [
      "bundle exec rake genpack:jobs"
    ]
    puts ssh_exec(Settings.manager, "cd #{GENPACK_REMOTE_LOCATION} && #{commands.join(' && ')}")

    # lock waiting experiment to be done
    begin
      open_gateway(Settings.manager, 6379)
      while Redis.current.get("genpack:experiment:running") == "1" do
        sleep 30
        puts "Experiment running for #{Time.at(Time.now - start).utc.strftime("%H:%M:%S").colorize(:yellow)}"
      end
    rescue Redis::ConnectionError, Redis::TimeoutError
      puts "Redis connection lost/timeout - retry"
      sleep 10
      retry
    end

    puts "Waiting #{SLEEP_AFTER} seconds before stopping scenario".colorize(:yellow)
    sleep SLEEP_AFTER

    finish = Time.now

    puts "Scenario done, all containers have been run (duration: #{finish - start}s)".colorize(:red)

    sleep SLEEP_MARGIN

    {
      start: start,
      finish: finish
    }
  end

  def run_histo(main)
    puts Settings.histo.strategies.inspect
    puts Settings.histo.timeouts.inspect

    start = Time.now
    threads = []

    Settings.histo.timeouts.each do |timeout|
      Settings.histo.strategies.each do |strategy|

        main.remove_all_containers
        main.monitor(strategy["nodes"])
        main.create_cluster(strategy["name"], strategy["nodes"])

        times = {}

        puts "Waiting #{SLEEP_BEFORE} seconds before starting scenario".colorize(:yellow)
        sleep SLEEP_BEFORE

        @scenario.each_with_index do |params, index|
          params[:timeout] = timeout
          params[:strategy] = strategy[:name]

          sleep params[:sleep]
          puts "\t--> Run container ##{index + 1}/#{@scenario.count} <--\n"

          threads << Thread.new do
            stressng = Stressng.new(params)

            puts "Run stressng with timeout #{params[:timeout]}s"

            start_stressng = Time.now
            puts stressng.run
            time_stressng = Time.now - start_stressng
            times[index] = time_stressng

            puts "Stressng done in #{time_stressng}s".colorize(:blue)

            puts "Container ##{index + 1} done at #{Time.now.to_s.colorize(:magenta)}"
          end
        end
        threads.each{ |thread| thread.join }

        puts "Waiting #{SLEEP_AFTER} seconds before stopping scenario".colorize(:yellow)
        sleep SLEEP_AFTER

        finish = Time.now

        puts "Scenario done, all containers have been run (duration: #{finish - start}s)".colorize(:red)
        puts times.inspect

        FileUtils.mkdir_p('output/histo')
        File.open("output/histo/#{timeout}_#{strategy["name"]}.dat", 'w') do |f|
          times.sort.to_h.each do |index, time|
            f.puts time
          end
        end
      end
    end

    {
      start: start,
      finish: finish
    }
  end




  def run_benchmarks(main)
    pp @scenario

    start = Time.now

    @scenario.each_with_index do |params, index|

      main.remove_all_containers
      main.create_nodes
      main.monitor
      main.create_cluster('spread')

      puts "\t--> Run benchmark ##{index + 1}/#{@scenario.count} <--\n"

      container = Container.new(
        Settings.manager,
        params[:image],
        params[:opts],
        params[:args],
        Settings.manager_localhost
      )

      start_container = Time.now

      puts "Waiting #{SLEEP_BEFORE_BENCH} seconds before starting benchmark".colorize(:yellow)
      sleep SLEEP_BEFORE_BENCH

      puts "Run #{params[:title]} with timeout #{params[:timeout]}s"
      incr_counter
      container.run
      decr_counter

      puts @containers.inspect

      puts "Waiting #{SLEEP_AFTER_BENCH} seconds before stopping monitoring".colorize(:yellow)
      sleep SLEEP_AFTER_BENCH

      finish_container = Time.now
      time_container = finish_container - start_container

      sleep SLEEP_MARGIN

      main.retrieve_datas({
        times: {
          start: start_container,
          finish: finish_container
        },
        name: "#{params[:title]} - #{params[:args]}",
        filename: params[:title],
        counters: @containers,
        output_directory: "benchmarks-#{start.strftime("%Y%m%d%H%M%S")}",
        gnuplot: {
          template: 'results_benchmarks.erb.gp',
          cpus: 8,
          memory: 8,
        }
      })
    end


    finish = Time.now

    puts "Scenario done, all containers have been run (duration: #{finish - start}s)".colorize(:red)
  end


  def export(filename)
    FileUtils.mkdir_p('output')
    File.open("output/#{filename}",'w') do |f|
      f.write(@scenario.to_yaml)
    end
  end

  def self.import(filename)
    self.new(YAML::load_file("output/#{filename}"))
  end

  def incr_counter type
    @counter[:global] += 1
    @counter[type.to_sym] += 1
    @containers[:global][Time.now.to_i] = @counter[:global]
    @containers[type.to_sym][Time.now.to_i] = @counter[type.to_sym]
  end

  def decr_counter type
    @counter[:global] -= 1
    @counter[type.to_sym] -= 1
    @containers[:global][Time.now.to_i] = @counter[:global]
    @containers[type.to_sym][Time.now.to_i] = @counter[type.to_sym]
  end
end
