require 'influxdb'
require 'erb'
require 'duration'
require_relative 'settings'
require_relative 'ssh'
require 'colorize'

class InfluxData
  attr_reader :duration, :results, :name, :filename, :start, :finish, :sampling, :gnuplot, :counters, :counters_by_group

  IDLE_POWER = 87.50325130890052 # given in Watts

  SERIES = {
    cpu_usage_total: {
      method: ->(values, i) {
        if i == 0
          0
        else
          (values[i]["value"] -  values[i-1]["value"]) / ((values[i]["time"] -  values[i-1]["time"]) * 1000000.0)
        end
      },
      scale: 1,
      unit: '#CPU',
      max: 40
    },
    # fs_usage: {
    #   query: ->(serie, start, finish) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY time(1s),machine" },
    #   method: ->(values, i) { values[i]["mean"] || values[i-1] && values[i-1]["mean"] || values[i-2] && values[i+2]["mean"] || 0 }
    # },
    # fs_limit: {
    #   query: ->(serie, start, finish) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY time(1s),machine" },
    #   method: ->(values, i) { values[i]["mean"] || values[i+1] && values[i+1]["mean"] || values[i+2] && values[i+2]["mean"] || 0 }
    # },
    # memory_usage: {
    #   query: ->(serie, start, finish) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY time(1s),machine" },
    #   method: ->(values, i) { ( values.lookback(i, 10) && values.lookback(i, 10)["mean"] || 0) / 2**20 },
    #   scale: 1024,
    #   unit: 'Go',
    #   max: 40
    # },
    memory_usage: {
      query: ->(serie, start, finish) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY time(1s),machine" },
      method: ->(values, i) { (
        values[i] && values[i]["mean"] ||
        values[i-1] && values[i-1]["mean"] ||
        values[i-2] && values[i-2]["mean"] ||
        values[i-3] && values[i-3]["mean"] ||
        values[i-4] && values[i-4]["mean"] ||
        values[i-5] && values[i-5]["mean"] ||
        0) / 2**20 },
      scale: 1024,
      unit: 'Go',
      max: 40
    },
    # memory_working_set: {
    #   query: ->(serie, start, finish) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY time(1s),machine" },
    #   method: ->(values, i) { ( values.lookback(i, 10) && values.lookback(i, 10)["mean"] || 0) / 2**20 },
    #   scale: 1024,
    #   unit: 'Go',
    #   max: 40
    # },
    power: {
      method: ->(values, i) { IDLE_POWER * 1000 + (values[i]["value"] || 0) }, # given in mWatts
      scale: 1000, # given in Watts
      unit: 'W',
      max: 1000
    }
  }
  DEFAULT = {
    query: ->(serie, start, finish) { "SELECT * FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY machine fill(0)" },
    method: ->(values, i) { values[i]["value"] }
  }
  SAMPLING = 1
  NODE_SERIES = {
    cpu_usage_total: {
      query: ->(serie, start, finish, machine) { "SELECT * FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' AND machine = '#{machine}' fill(0)" },
      method: ->(values, i) {
        if i == 0
          0
        else
          (values[i]["value"] - values[i-1]["value"]) / ((values[i]["time"] -  values[i-1]["time"]) * 1000000.0)
        end
      },
      scale: 1,
      unit: '#CPU',
      max: 8
    },
    memory_usage: {
      query: ->(serie, start, finish, machine) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' AND machine = '#{machine}' GROUP BY time(1s)" },
      method: ->(values, i) { (
        values[i] && values[i]["mean"] ||
        values[i-1] && values[i-1]["mean"] ||
        values[i-2] && values[i-2]["mean"] ||
        values[i-3] && values[i-3]["mean"] ||
        values[i-4] && values[i-4]["mean"] ||
        values[i-5] && values[i-5]["mean"] ||
        0) / 2**20 },
      scale: 1024,
      unit: 'Go',
      max: 8
    },
    # memory_working_set: {
    #   query: ->(serie, start, finish) { "SELECT mean(value) FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' GROUP BY time(1s),machine" },
    #   method: ->(values, i) { ( values.lookback(i, 10) && values.lookback(i, 10)["mean"] || 0) / 2**20 },
    #   scale: 1024,
    #   unit: 'Go',
    #   max: 40
    # },
    power: {
      query: ->(serie, start, finish, machine) { "SELECT * FROM #{serie} WHERE time > #{start.to_i}s AND time < #{finish.to_i}s AND container_name = '/' AND machine = '#{machine}' fill(0)" },
      method: ->(values, i) { IDLE_POWER * 1000 + (values[i]["value"] || 0) }, # given in mWatts
      scale: 1000, # given in Watts
      unit: 'W',
      max: 1000
    }
  }

  # duration is a Int number of seconds
  def initialize(params={})
    params[:times] ||= {}

    @start = params[:times][:start] || 0
    @finish = params[:times][:finish] || 0
    @duration = Duration.new(@finish - @start)
    puts @duration.inspect
    @results = {}
    @node_results = {}
    @name = params[:name] || ''
    @filename = params[:filename] || params[:name] || ''
    @sampling = SAMPLING
    @counters = params[:counters] || {}
    @gnuplot = params[:gnuplot] || {}

    puts @counters.inspect

    @output_directory = "output/#{params[:output_directory]}"

    @machines = params[:machines] || []
  end

  def retrieve_influx_data
    queries = SERIES.map do |serie, options|
      (options[:query] || DEFAULT[:query]).call(serie, @start, @finish)
    end

    queries.each do |query|
      puts "Run query #{query.colorize(:yellow)}"
      influxdb.query(query).each do |data|
        name = data["name"].to_sym
        # puts data.inspect.colorize(:blue)
        previous = nil
        data["values"].each_with_index do |value, i|
          timestamp = value["time"] / 1000 # convert timestamp from millisecond to second
          @results[timestamp] ||= {}
          @results[timestamp][name] = (@results[timestamp][name] || 0) + ((SERIES[name][:method] || DEFAULT[:method]).call(data["values"], i) || 0)
        end
      end
    end

    @machines.each do |machine|
      queries = NODE_SERIES.map do |serie, options|
        (options[:query] || DEFAULT[:query]).call(serie, @start, @finish, machine[:id])
      end

      queries.each do |query|
        puts "Run query #{query.colorize(:yellow)}"
        influxdb.query(query).each do |data|
          name = data["name"].to_sym
          puts data.inspect[0..400].gsub(/\s\w+\s*$/, '...').colorize(:blue)
          previous = nil
          data["values"].each_with_index do |value, i|
            timestamp = value["time"] / 1000 # convert timestamp from millisecond to second
            @node_results[timestamp] ||= {}
            @node_results[timestamp][machine[:id]] ||= {}
            @node_results[timestamp][machine[:id]][name] = (NODE_SERIES[name][:method] || DEFAULT[:method]).call(data["values"], i) || 0
          end
        end
      end
    end

    # puts @node_results.inspect
  end

  def retrieve_genpack_datas
    command = "#{Lookbusy::RAKE} genpack:counters"
    puts command
    datas = JSON.parse ssh_exec(Settings.manager, "cd #{Lookbusy::WORKDIR} && #{command}").uncolorize

    # Settings.nodes.each{ |node| ssh_exec(node.ip, set_hostname.call(node.name)) }
    timestamps = @results.keys
    @genpack_counters = datas.reject{ |data| data["time"] < @start.to_i || data["time"] > @finish.to_i }

    groups = {}
    Settings.nodes.each{ |node| (groups[node["group"]] ||= []) << node["name"] }

    @counters_by_group = groups.map { |group,nodes| [group, counters_for_nodes(nodes)] }.to_h

    puts @counters_by_group.inspect[0..400].gsub(/\s\w+\s*$/, '...').colorize(:green)
  end

  def counters_for_nodes(nodes)
    result = {}

    @genpack_counters.each do |counter|
      entry_for_node =
      ((result[counter["type"]] ||= {})[counter["node"]] ||= {})[counter["time"]] = counter["value"] if nodes.include? counter["node"]
    end

    result
  end

  def retrieve_data
    retrieve_influx_data
    retrieve_genpack_datas

    export

    self
  end

  def to_graph
    # raise 'No data... maybe you have to run #retrieve_data first' if @results.empty?

    gnuplot_script
    `cd #{@output_directory} && gnuplot "#{@filename}.gp"`
  end

  # private

    def influxdb
      open_gateway(Settings.manager, 8086)
      @influxdb_client ||= client
    end

    def client
      InfluxDB::Client.new 'cadvisor', epoch: 'ms', retry: 10
    end

    def export
      FileUtils.mkdir_p(@output_directory)
      File.open("#{@output_directory}/#{@filename}.dat", 'w') do |f|
        f.puts [
          '#',
          'timestamp',
          SERIES.keys,
          @counters.keys.map{ |k| "counter_#{k.to_s}" },
          @counters_by_group.map{ |group, types| types.keys.map{ |type| "#{group}_#{type}" } },
          @counters_by_group.map{ |group, types| types.map{ |type,nodes| nodes.map{ |node, datas| "#{node}_#{type}" } } },
          @machines.map{ |machine| NODE_SERIES.keys.map{ |k| "#{machine["name"]}-#{k.to_s}" } }
        ].flatten.join(' ')

        @results.sort.to_h.each_with_index do |(time, datas), index|
          f.puts [
            index,
            time,
            SERIES.keys.map{ |s| datas[s] || 0 },
            @counters.map{ |k,v| v.to_a.reverse.detect(->{[0,0]}){ |c| c.first < time }.last },
            @counters_by_group.map{ |group,types|
              types.map{ |type,nodes|
                nodes.inject(0){ |sum,(node,values)|
                  sum + values.to_a.reverse.detect(->{[0,0]}){ |c|
                    c.first < time
                  }.last
                }
              }
            },
            @counters_by_group.map{ |group,types|
              types.map{ |type,nodes|
                nodes.map{ |node, values|
                  values.to_a.reverse.detect(->{[0,0]}){ |c|
                    c.first < time
                  }.last
                }
              }
            },
            @machines.map{ |machine| NODE_SERIES.keys.map{ |k| @node_results[time][machine[:id]][k] || @node_results[time-1] && @node_results[time-1][machine[:id]][k] || @node_results[time-2] && @node_results[time-2][machine[:id]][k] || 0 } }
          ].flatten.join(' ')
          # puts @machines.map{ |machine| NODE_SERIES.keys.map{ |k| @node_results[time][machine[:id]][k] } }.inspect
          # puts @machines.map{ |machine| NODE_SERIES.keys.map{ |k| @node_results[time][machine[:id]][k] || 0 } }.inspect
        end
      end
    end

    def gnuplot_script
      filename = "templates/#{@gnuplot[:template] || 'results.erb.gp'}"
      erb = ERB.new(File.read(filename))
      erb.filename = filename
      gnuplot_klass = erb.def_class(Gnuplot_, 'render()')

      File.open("#{@output_directory}/#{@filename}.gp", 'w') do |f|
        f.write gnuplot_klass.new(self).render()
      end
    end

    class Gnuplot_
      def initialize(influxdata)
        @series = SERIES
        @name = influxdata.name
        @filename = influxdata.filename
        @duration = influxdata.duration.total / influxdata.sampling
        xtics_number = 5
        @xtics = (0..xtics_number).map{ |i| "\"#{(i * @duration.to_f * influxdata.sampling / 60 / xtics_number).round(1)}m\" #{i * @duration / xtics_number}" }.join(',')
        @settings = Settings
        @params = influxdata.gnuplot
        @counters = influxdata.counters
        @counters_by_group = influxdata.counters_by_group
      end
    end
end

Hash.class_eval do
  def lookback(index, steps)
    i = index
    until self[i] || i == (index - steps) do
      i -= 1
    end
    self[i] || 0
  end
end
