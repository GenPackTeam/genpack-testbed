class Cpuset < Container
  IMAGE = 'agileek/cpuset-test:latest'

  @@run_cpuset_container = ->(){ docker[:run].call(Settings.manager_localhost, '-d --cpuset-cpus=0 --cpu-shares 1 --memory 500M', IMAGE) }

  def initialize
    super(Settings.manager, @@run_cpuset_container.call())
  end
end
