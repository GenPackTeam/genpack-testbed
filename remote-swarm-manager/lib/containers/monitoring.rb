class Influxdb < Container
  def initialize
    influx_image = 'tutum/influxdb:0.13'
    influx_opts = '--volume=/mnt/store/influxdb:/data --restart=always --publish=8083:8083 --publish=8086:8086 -e ADMIN_USER="root" -e INFLUXDB_INIT_PWD="root" -e PRE_CREATE_DB=cadvisor --name=InfluxSrv'

    ssh_exec(Settings.manager, docker[:rm].call(localhost, '-f -v', 'InfluxSrv'))

    super(Settings.manager, influx_image, influx_opts)
  end
end

class Grafana < Container
  def initialize
    grafana_image = 'grafana/grafana:2.0.2'
    grafana_opts = '--publish=3000:3000 -e INFLUXDB_HOST=localhost -e INFLUXDB_PORT=8086 -e INFLUXDB_NAME=cadvisor -e INFLUXDB_USER=root -e INFLUXDB_PASS=root --link="InfluxSrv:influxsrv" --name=GrafanaSrv'

    super(Settings.manager, grafana_image, grafana_opts)
  end
end

class Cadvisor < Container
  def initialize(target)
    cadvisor_image = 'lorel/cadvisor:beta'
    cadvisor_opts = "--volume=/:/rootfs:ro --volume=/var/run:/var/run:rw --volume=/sys:/sys:ro --volume=/var/lib/docker/:/var/lib/docker:ro --publish=8080:8080 --name=cadvisor --cidfile=\"/home/#{Settings.ssh.user}/cadvisor.cid\" --cpuset-cpus=0"
    cadvisor_args = "-storage_driver=influxdb -storage_driver_db=cadvisor -storage_driver_host=#{Settings.manager}:8086 -allow_dynamic_housekeeping=false -max_housekeeping_interval=30s -disable_metrics disk,network,tcp"
    # cadvisor_args = "-storage_driver=influxdb -storage_driver_db=cadvisor -storage_driver_host=#{Settings.manager}:8086 -allow_dynamic_housekeeping=false -max_housekeeping_interval=30s"

    puts ssh_exec(target, "rm -rfv /home/#{Settings.ssh.user}/cadvisor.cid")

    super(target, cadvisor_image, cadvisor_opts, cadvisor_args)
  end

  def run
    ssh_exec(@target, docker[:rm].call(localhost, '-f -v', 'cadvisor'))
    super
    sleep 1
    ssh_exec(@target, "sudo renice -n -10 $(pgrep cadvisor)")
  end
end

class PowerAPI < Container
  def initialize(target)
    powerapi_image = 'spirals/powerapi-osdi'
    powerapi_opts = "--volume=/home/#{Settings.ssh.user}/cadvisor.cid:/tmp/cadvisor.cid:ro --volume=/sys:/sys:ro --volume=/var/run:/var/run:rw --volume=/var/lib/docker/:/var/lib/docker:ro --name=powerapi --cpuset-cpus=0"
    powerapi_args = "--tdp 50 --influx_host \"#{Settings.manager}\" --influx_port \"8086\" --influx_user root --influx_pwd root --cadvisor_cid_path \"/tmp/cadvisor.cid\""

    super(target, powerapi_image, powerapi_opts, powerapi_args)
  end

  def run
    ssh_exec(@target, docker[:rm].call(localhost, '-f -v', 'powerapi'))
    super
  end
end
