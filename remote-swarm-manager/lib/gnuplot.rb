class Gnuplot
  attr_reader :datas, :name, :duration, :sampling

  def initialize(datas = [], output_directory)
    @datas = datas
    @name = @datas.map{ |d| d.name }.join('-')
    @duration = @datas.map{ |d| d.duration }.max_by(&:total)

    @output_directory = "output/#{output_directory || 'undefined'}"
  end

  def to_graph
    raise 'No data... maybe you have to run #retrieve_data first' if @datas.empty?

    gnuplot_script
    `cd #{@output_directory} && gnuplot "#{@name}.gp"`
  end

  def gnuplot_script
    File.open("#{@output_directory}/#{@name}.gp", 'w') do |f|
      f.write GnuplotScript.new(self).render()
    end
  end

  class GnuplotScript_
    def initialize(gnuplot)
      @series = InfluxData::SERIES.keys
      @filenames = gnuplot.datas.map{ |d| d.filename }
      @name = gnuplot.name
      @duration = gnuplot.duration.total / InfluxData::SAMPLING
      xtics_number = 5
      @xtics = (0..xtics_number).map{ |i| "\"#{(i * @duration.to_f * InfluxData::SAMPLING / 60 / xtics_number).round(1)}m\" #{i * @duration / xtics_number}" }.join(',')
      @settings = Settings
    end
  end

  filename = 'templates/gnuplot_script.erb.gp'
  erb = ERB.new(File.read(filename))
  erb.filename = filename
  GnuplotScript = erb.def_class(GnuplotScript_, 'render()')
end
