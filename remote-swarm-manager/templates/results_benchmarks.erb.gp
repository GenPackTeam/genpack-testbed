set term postscript color portrait size 14, <%= (@series.count + 1) * 4 %>
set output "<%= @filename %>.eps"
load "../styles.inc"

set multiplot layout <%= @series.count + 1 %>, <%= @sliding ? 2 : 1 %> title "<%= @name %>"
set bmargin 2
set tmargin 3
set lmargin 15
set rmargin 2

set grid x front
set xtics (<%= @xtics %>)
set xrange [0:<%= @duration %>]

set grid y
set autoscale y

<% @series.keys.each_with_index do |serie, index| %>
set ylabel "<%= serie.to_s.gsub("_", " ") %> (<%= @series[serie][:unit] %>)" offset 1,0
set yrange [0:<%= @series[serie][:max] %>]
plot '<%= @filename %>.dat' using 1:($<%= index + 3 %>/<%= @series[serie][:scale] %>) with lines ls 1 notitle
<% end %>

set ylabel "#containers" offset 1,0
unset yrange
plot '<%= @filename %>.dat' using 1:($7) with lines ls 1 notitle

!epstopdf "<%= @filename %>.eps"
!rm "<%= @filename %>.eps"
quit
