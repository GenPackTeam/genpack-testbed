set term postscript color portrait size 14, <%= (@series.count + 1) * 4 %>
set output "<%= @name %>.eps"
load "../styles.inc"

set multiplot layout <%= @series.count + 1 %>, 1  title "<%= @name %>"
set bmargin 2
set tmargin 3
set lmargin 15
set rmargin 2

set grid x front
set xtics (<%= @xtics %>)
set xrange [0:<%= @duration %>]

set grid y
set autoscale y

<% @series.each_with_index do |serie, index| %>
set ylabel "<%= serie.to_s.gsub("_", " ") %>" offset 1,0
plot \
<% @filenames.each_with_index do |filename, i| %>  '<%= filename %>.dat' using 1:($<%= index + 3 %>) with lines ls <%= i + 1 %> title '<%= filename %>', \
<% end %>

<% end %>

set ylabel "#containers" offset 1,0
plot \
<% @filenames.each_with_index do |filename, i| %>  '<%= filename %>.dat' using 1:($<%= @series.size + 3 %>) with lines ls <%= i + 1 %> title '<%= filename %>', \
<% end %>


!epstopdf "<%= @name %>.eps"
!rm "<%= @name %>.eps"
quit
